// Karma configuration
// http://karma-runner.github.io/0.12/config/configuration-file.html
// Generated on 2014-10-10 using
// generator-karma 0.8.3

process.env.CHROME_BIN = require('puppeteer').executablePath();

module.exports = function(config) {
  'use strict';

  config.set({
    // base path, that will be used to resolve files and exclude
    basePath: '../',

    // testing framework to use (jasmine/mocha/qunit/...)
    frameworks: ['browserify', 'jasmine'],

    // list of files / patterns to load in the browser
    files: [
      'node_modules/chart.js/dist/Chart.min.js',
      'node_modules/file-saver/dist/FileSaver.min.js',
      'node_modules/golden-layout/dist/goldenlayout.min.js',
      'node_modules/json3/lib/json3.js',
      'node_modules/jszip/dist/jszip.js',
      'node_modules/rxjs/bundles/Rx.min.js',
      'node_modules/stats.js/build/stats.min.js',

      // The two following files are required by gz3d.js. Since
      // order matters here, they should be in front of the bower
      // section.

      'node_modules/three/build/three.js',
      'node_modules/three/examples/js/postprocessing/EffectComposer.js',

      'node_modules/jquery/dist/jquery.js',

      'node_modules/angular/angular.js',
      'node_modules/angular-animate/angular-animate.js',
      'node_modules/angular-aria/angular-aria.js',
      'node_modules/angular-bootstrap-contextmenu/contextMenu.js',
      'node_modules/angular-cookies/angular-cookies.js',
      'node_modules/angular-messages/angular-messages.js',
      'node_modules/angular-material/angular-material.js',
      'node_modules/angular-resource/angular-resource.js',
      'node_modules/angular-sanitize/angular-sanitize.js',
      'node_modules/angular-toarrayfilter/toArrayFilter.js',
      'node_modules/angular-touch/angular-touch.js',
      'node_modules/codemirror/lib/codemirror.js',
      'node_modules/codemirror/mode/python/python.js',
      'node_modules/codemirror/mode/shell/shell.js',
      'node_modules/angular-ui-codemirror/src/ui-codemirror.js',
      'node_modules/@uirouter/angularjs/release/angular-ui-router.js',
      'node_modules/angular-smart-table/dist/smart-table.js',
      'node_modules/ng-file-upload/dist/ng-file-upload.js',
      'node_modules/angulartics/src/angulartics.js',
      'node_modules/angulartics-google-analytics/lib/angulartics-ga.js',
      'node_modules/moment/moment.js',
      'node_modules/angular-uuid4/angular-uuid4.js',
      'node_modules/angular-deferred-bootstrap/angular-deferred-bootstrap.js',
      'node_modules/angular-moment/angular-moment.js',
      'node_modules/angular-ui-bootstrap/dist/ui-bootstrap-tpls.js',

      // The lines between 'bower:' and 'endbower' are filled automagically with the respective bower components.
      // Note though that several packages are excluded (as described in the Gruntfile.js).

      // bower:'bower_components/jquery/dist/jquery.js',
      // endbower

      // manual bower dependency additions while transitioning to NPM, some NPM packages as ordering is important
      'bower_components/angular-bbp-config/angular-bbp-config.js',
      'bower_components/angular.panels/dist/angular.panels.min.js',
      'node_modules/es5-shim/es5-shim.js',
      'node_modules/eventemitter2/lib/eventemitter2.js',
      'bower_components/xml2json/xml2json.js',
      'node_modules/roslib/src/RosLibBrowser.js',
      'node_modules/lodash/lodash.js',
      'bower_components/gz3d-hbp/gz3d/build/gz3d.js',
      'bower_components/gz3d-hbp/gz3d/client/js/include/ColladaLoader.js',
      'bower_components/gz3d-hbp/gz3d/client/js/include/lookat-controls.js',
      'bower_components/gz3d-hbp/gz3d/client/js/include/avatar-controls.js',
      'bower_components/gz3d-hbp/gz3d/client/js/include/ThreeBackwardsCompatibility.js',
      'bower_components/abdmob/x2js/xml2json.min.js',
      'bower_components/SHA-1/sha1.js',
      'bower_components/brainvisualizer/src/brain3D.js',
      'bower_components/brainvisualizer/src/brain3Dmainview.js',
      'bower_components/brainvisualizer/src/brain3Dneuronrepresentation.js',
      'bower_components/brainvisualizer/shaders/brain3Dshaders.js',

      'node_modules/angular-mocks/angular-mocks.js',
      'node_modules/angular-tree-control/angular-tree-control.js',
      'node_modules/d3/d3.min.js',
      'node_modules/n3-charts/build/LineChart.js',
      'node_modules/v-button/dist/v-button.js',
      'test/support/**/*.js',
      'app/components/**/*.modules.js', // files defining modules first
      'app/scripts/common/filters/time-filters.js', // Make sure modules used in different files are loaded before they are used
      'app/scripts/**/*.js',
      'app/components/**/*.js',
      'test/mock/**/*.js',
      'test/spec/datagenerator/*.js',
      'test/spec/**/*.js',
      'app/components/**/*.html',
      'app/views/**/*.html',

      { pattern: 'app/views/*.*', included: true, served: true }
    ],

    preprocessors: {
      // source files, that you want to generate coverage for
      // do not include tests or libraries
      // (these files will be instrumented by Istanbul)
      'app/scripts/*/**/*.js': ['coverage'],
      'app/components/**/*.js': ['coverage'],
      'app/components/**/*.html': ['ng-html2js'],
      'app/views/**/*.html': ['ng-html2js'],
      'node_modules/roslib/src/RosLibBrowser.js': ['browserify']
    },

    ngHtml2JsPreprocessor: {
      stripPrefix: 'app/',
      prependPrefix: '',

      // the name of the Angular module to create
      moduleName: 'exd.templates'
    },

    // list of files / patterns to exclude
    exclude: [],

    // web server port
    port: 9002,

    browsers: ['ChromeHeadlessNoSandbox'],
    reporters: ['progress', 'junit', 'coverage'],
    customLaunchers: {
      ChromeHeadlessNoSandbox: {
        base: 'ChromeHeadless',
        flags: ['--no-sandbox']
      }
    },

    // Which plugins to enable
    plugins: [
      'karma-chrome-launcher',
      'karma-jasmine',
      'karma-coverage',
      'karma-junit-reporter',
      'karma-ng-html2js-preprocessor',
      'karma-spec-reporter',
      'karma-browserify'
    ],

    specReporter: {
      maxLogLines: 5, // limit number of lines logged per test
      suppressErrorSummary: false, // do not print error summary
      suppressFailed: false, // do not print information about failed tests
      suppressPassed: true, // do not print information about passed tests
      suppressSkipped: true, // do not print information about skipped tests
      showSpecTiming: true // print the time elapsed for each spec
    },

    junitReporter: {
      outputDir: 'reports/coverage/',
      outputFile: 'unit-test.xml',
      suite: 'unit'
    },

    coverageReporter: {
      instrumenterOptions: {
        istanbul: { noCompact: true }
      },
      reporters: [
        {
          type: 'lcov',
          dir: 'reports/coverage/',
          file: 'coverage.info'
        },
        {
          type: 'cobertura',
          dir: 'reports/coverage/'
        }
      ]
    },

    // Continuous Integration mode
    // if true, it capture browsers, run tests and exit
    singleRun: false,

    colors: true,

    proxies: {
      '/views/': 'http://localhost:8000/views/',
      '/img/': 'app/img/'
    }

    // Uncomment the following lines if you are using grunt's server to run the tests
    // proxies: {
    //   '/': 'http://localhost:9000/'
    // },
    // URL root prevent conflicts with the site root
    // urlRoot: '_karma_'
  });
};
