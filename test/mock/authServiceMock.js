(function() {
  'use strict';

  const mockToken = '123456789abcdefghijklmnopqrstuvwxyz';

  angular.module('authServiceMock', []).service('authService', function() {
    this.authenticate = jasmine.createSpy('authenticate');
    this.getToken = jasmine.createSpy('getToken').and.returnValue(mockToken);
    this.logout = jasmine.createSpy('logout');
    this.initialized = true;
    this.promiseInitialized = Promise.resolve();
  });
})();
