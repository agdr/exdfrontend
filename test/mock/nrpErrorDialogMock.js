(function() {
  'use strict';

  angular
    .module('nrpErrorDialogMock', [])
    .service('nrpErrorDialog', function() {
      this.open = jasmine.createSpy('open');
    });
})();
