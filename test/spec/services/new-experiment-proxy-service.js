'use strict';

describe('Services: new-experiment-proxy-service', function() {
  var bbpConfig, newExperimentProxyService, proxyUrl;
  // load the service to test and mock the necessary service
  beforeEach(module('exdFrontendApp'));
  beforeEach(module('exd.templates'));
  beforeEach(
    inject(function(_bbpConfig_, _newExperimentProxyService_) {
      bbpConfig = _bbpConfig_;
      newExperimentProxyService = _newExperimentProxyService_;
      proxyUrl = bbpConfig.get('api.proxy.url');
    })
  );

  it('should make sure the get proxy url returns the correct url', function() {
    spyOn(bbpConfig, 'get').and.callThrough();
    var testProxyUrl = newExperimentProxyService.getProxyUrl();
    expect(bbpConfig.get).toHaveBeenCalledWith('api.proxy.url');
    expect(testProxyUrl).toEqual(proxyUrl);
  });
});
