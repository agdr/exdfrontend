'use strict';

describe('Services: export-experiment', () => {
  let exportExperimentService,
    downloadFileService,
    $rootScope,
    storageServer,
    $q;
  beforeEach(module('exdFrontendApp'));
  beforeEach(
    inject(function(
      _exportExperimentService_,
      _downloadFileService_,
      _$rootScope_,
      _storageServer_,
      _$q_
    ) {
      exportExperimentService = _exportExperimentService_;
      downloadFileService = _downloadFileService_;
      $rootScope = _$rootScope_;
      storageServer = _storageServer_;
      $q = _$q_;
    })
  );

  it('should get a zip from the storage server', () => {
    spyOn(downloadFileService, 'downloadFromURL');
    spyOn(storageServer, 'getZip').and.returnValue(
      $q.resolve({ data: 'testData' })
    );
    exportExperimentService.getZip('zipPath', 'zipName', 'expUUID');
    $rootScope.$apply();
    expect(downloadFileService.downloadFromURL.calls.mostRecent().args[1]).toBe(
      'zipName'
    );
    expect(storageServer.getZip).toHaveBeenCalledWith('zipPath', 'zipName');
  });

  it('should download experiment zips', () => {
    spyOn(exportExperimentService, 'getZip').and.returnValue($q.resolve());
    exportExperimentService.downloadExperimentZips(
      {
        envZip: { path: 'envPath.zip' },
        experimentZip: { path: '/tmp/somePath.zip' },
        robotZips: [{ path: 'robotPath.zip', name: 'robot' }]
      },
      'zipName',
      'expUUID'
    );
    $rootScope.$apply();
    expect(exportExperimentService.getZip.calls.mostRecent().args).toEqual([
      'robotPath.zip',
      'robot'
    ]);
  });
});
