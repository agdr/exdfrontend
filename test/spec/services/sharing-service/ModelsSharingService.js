'use strict';

describe('Services: ExperimentsSharingService', function() {
  let storageServer, modelSharingService, $rootScope;

  beforeEach(module('exdFrontendApp'));
  beforeEach(module('storageServerMock'));
  beforeEach(module('nrpModalServiceMock'));
  beforeEach(
    inject(function(
      _storageServer_,
      _nrpErrorDialog_,
      _$rootScope_,
      _modelSharingService_
    ) {
      $rootScope = _$rootScope_;
      storageServer = _storageServer_;
      modelSharingService = _modelSharingService_;
      modelSharingService.storageServer = storageServer;
    })
  );

  it('should update the shared option for the model', function() {
    modelSharingService.updateSharedEntityMode('private');
    expect(storageServer.updateSharedModelMode).toHaveBeenCalled();
  });

  it('should not update the shared option for the model', function() {
    spyOn(console, 'error');
    storageServer.itWasSuccessful = false;
    modelSharingService.updateSharedEntityMode('private');
    $rootScope.$digest();
    expect(storageServer.updateSharedModelMode).toHaveBeenCalled();
    expect(console.error).toHaveBeenCalled();
  });

  it('should not delete  if the shared option is not private', function() {
    modelSharingService.model = { entitySharingMode: 'Public' };
    modelSharingService.deleteSharedEntityUser('user1');
    expect(storageServer.deleteSharedModelUser).not.toHaveBeenCalled();
  });

  it('should delete a shared user', function() {
    modelSharingService.model = {
      entitySharingMode: 'Private',
      entityModeSharedOption: 'Private'
    };
    modelSharingService.deleteSharedEntityUser('user1');
    $rootScope.$digest();
    expect(storageServer.deleteSharedModelUser).toHaveBeenCalled();
  });

  it('should not delete a shared user', function() {
    spyOn(console, 'error');
    storageServer.itWasSuccessful = false;
    modelSharingService.model = {
      entitySharingMode: 'Private',
      entityModeSharedOption: 'Private'
    };
    modelSharingService.deleteSharedEntityUser('user1');
    $rootScope.$digest();
    expect(console.error).toHaveBeenCalled();
  });

  it('should get shared entity users', function() {
    modelSharingService.model = {
      allUsers: [
        {
          displayName: 'user0',
          id: 'user0',
          username: 'user0'
        }
      ]
    };
    modelSharingService.getSharedEntityUsers('user0');
    $rootScope.$digest();
    expect(modelSharingService.model.sharingUsers[0].id).toBe('user0');
  });

  it('should not get shared entity users', function() {
    modelSharingService.model = {
      allUsers: [
        {
          displayName: 'user0',
          id: 'user0',
          username: 'user0'
        }
      ]
    };
    spyOn(console, 'error');
    storageServer.itWasSuccessful = false;
    modelSharingService.getSharedEntityUsers('user0');
    $rootScope.$digest();
    expect(console.error).toHaveBeenCalled();
  });

  it('should get the correct Model shared option', function() {
    modelSharingService.getSharedEntityMode();
    $rootScope.$digest();
    expect(modelSharingService.model.entitySharingMode).toBe('Private');
  });

  it('should not get the correct Model shared option', function() {
    spyOn(console, 'error');
    storageServer.itWasSuccessful = false;
    modelSharingService.getSharedEntityMode();
    $rootScope.$digest();
    expect(console.error).toHaveBeenCalled();
  });

  it('should update shared user list', function() {
    modelSharingService.model = {
      allUsers: [
        {
          displayName: 'user1',
          id: 'user1',
          username: 'user1'
        }
      ]
    };
    spyOn(modelSharingService, 'getSharedEntityUsers').and.callThrough();
    modelSharingService.selectedUserChange({ searchUser: 'user1' });
    $rootScope.$digest();
    expect(storageServer.addSharedModelUsers).toHaveBeenCalled();
  });

  it('should not update shared user list', function() {
    spyOn(console, 'error');
    storageServer.itWasSuccessful = false;
    modelSharingService.model = {
      allUsers: [
        {
          displayName: 'user1',
          id: 'user1',
          username: 'user1'
        }
      ]
    };
    spyOn(modelSharingService, 'getSharedEntityUsers').and.callThrough();
    modelSharingService.selectedUserChange({ searchUser: 'user1' });
    $rootScope.$digest();
    expect(storageServer.addSharedModelUsers).toHaveBeenCalled();
    expect(console.error).toHaveBeenCalled();
  });

  it('should not update shared user list', function() {
    spyOn(console, 'error');
    storageServer.itWasSuccessful = false;
    modelSharingService.model = {
      allUsers: [
        {
          displayName: 'user1',
          id: 'user1',
          username: 'user1'
        }
      ]
    };
    spyOn(modelSharingService, 'getSharedEntityUsers').and.callThrough();
    modelSharingService.selectedUserChange({ searchUser: 'user1' });
    $rootScope.$digest();
    expect(storageServer.addSharedModelUsers).toHaveBeenCalled();
    expect(console.error).toHaveBeenCalled();
  });
});
