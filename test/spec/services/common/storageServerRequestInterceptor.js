(function() {
  'use strict';

  describe('Service: StorageServerRequestInterceptor', function() {
    beforeEach(module('storageServer'));
    beforeEach(module('authServiceMock'));

    let windowMock = {
      location: {
        href: null
      }
    };

    beforeEach(
      module(function($provide) {
        $provide.value('$window', windowMock);
      })
    );

    let storageServerRequestInterceptor, authService;
    beforeEach(
      inject(function(_storageServerRequestInterceptor_, _authService_) {
        storageServerRequestInterceptor = _storageServerRequestInterceptor_;
        authService = _authService_;
      })
    );

    it('should handle maintenance mode', function() {
      spyOn(storageServerRequestInterceptor.$location, 'path');
      storageServerRequestInterceptor.responseError({ status: 478 });
      expect(
        storageServerRequestInterceptor.$location.path
      ).toHaveBeenCalledWith('maintenance');
    });

    it('should handle login url', function() {
      storageServerRequestInterceptor.responseError({ status: 477 });
      expect(authService.authenticate).toHaveBeenCalled();
    });
  });
})();
