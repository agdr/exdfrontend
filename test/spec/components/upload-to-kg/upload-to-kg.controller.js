(function() {
  'use strict';

  describe('Controller: UploadToKgController', function() {
    beforeEach(module('exdFrontendApp'));
    beforeEach(module('exd.templates'));
    beforeEach(module('simulationInfoMock'));
    beforeEach(module('nrpUserMock'));
    beforeEach(module('experimentServiceMock'));
    beforeEach(module('uploadToKgServiceMock'));
    beforeEach(module('kgInterfaceServiceMock'));

    let $rootScope, element, controller, storageServer, simulationInfo;
    let experimentService, nrpUser, uploadToKgService;
    let $q;

    const MOCKED_DATA = {
      experiments: [
        {
          uuid: '758096b6-e500-452b-93e1-ba2bba203844',
          name: 'New experiment',
          parent: '89857775-6215-4d53-94ee-fb6c18b9e2f8'
        }
      ],
      experimentFiles: [
        {
          uuid: '207a87c9-78d9-4504-bde7-6919feaac12c',
          name: 'all_spikes.csv',
          parent: '758096b6-e500-452b-93e1-ba2bba203844',
          contentType: 'text/plain',
          type: 'file',
          modifiedOn: '2017-08-07T07:59:35.837002Z',
          extension: 'csv',
          artifactType: 'spikes'
        },
        {
          uuid: '207a87c9-78d9-4504-bde7-6919feaac12e',
          name: 'all_spikes2.csv',
          parent: '758096b6-e500-452b-93e1-ba2bba203844',
          contentType: 'text/plain',
          type: 'file',
          modifiedOn: '2017-08-07T07:59:35.837002Z',
          extension: 'csv',
          artifactType: 'spikes'
        },
        {
          uuid: '758096b6-e500-452b-93e1-ba2bba203844',
          name: 'csv_records_2019-09-10_13-52-06',
          parent: '758236b6-e500-452b-93e1-ba2bba20323',
          type: 'folder',
          modifiedOn: '2017-08-07T07:59:35.837002Z'
        },
        {
          uuid: '207a87c9-78d9-4504-bde7-6919feaac12b',
          name: 'csv_records_2019-08-30_15-50-16',
          parent: '758236b6-e500-452b-93e1-ba2bba20323',
          type: 'folder',
          modifiedOn: '2017-08-07T07:59:35.837002Z'
        }
      ],
      simulation: {
        '@id':
          'https://nexus-int.humanbrainproject.org/v0/data/sp10/core/simulation/v1.0.0/9d4ea936-1426-4597-a9d6-9a3d669ae557',
        revision: 1
      },
      artifacts: [
        {
          '@id':
            'https://nexus-int.humanbrainproject.org/v0/data/sp10/artifacts/muscleforces/v1.0.0/25799a86-6ddf-43ce-9b9b-1ebf75c629a1',
          revision: 1,
          uploadedDate: ''
        }
      ]
    };

    beforeEach(
      inject(function(
        $controller,
        $compile,
        _$rootScope_,
        _$q_,
        _storageServer_,
        _simulationInfo_,
        _nrpUser_,
        _experimentService_,
        _uploadToKgService_
      ) {
        $q = _$q_;
        $rootScope = _$rootScope_;
        storageServer = _storageServer_;
        simulationInfo = _simulationInfo_;
        experimentService = _experimentService_;
        nrpUser = _nrpUser_;
        uploadToKgService = _uploadToKgService_;

        experimentService.creationDate = '1568896862119';
        spyOn(storageServer, 'getExperimentFiles').and.returnValue(
          $q.when(MOCKED_DATA.experimentFiles)
        );

        element = $compile('<upload-to-kg></upload-to-kg>')($rootScope);
        $rootScope.$digest();
        controller = element.scope().vm1;
        controller.$scope.vm.checkAndLoadCsvFolder();
        $rootScope.$digest();

        MOCKED_DATA.experimentFiles[0].artifactType = 'spikes';
        MOCKED_DATA.experimentFiles[1].artifactType = 'spikes';
      })
    );

    it('should set meta data', function() {
      const result = {
        experimentID: simulationInfo.experimentID,
        realTimeText: simulationInfo.realTimeText,
        creationDate: moment(experimentService.creationDate)
          .utc()
          .format('YYYY-MM-DD HH:mm:ss'),
        name: simulationInfo.experimentDetails.name
      };
      nrpUser.getOwnerDisplayName('me').then(owner => {
        result.owner = owner;
        expect(controller.metaData).toEqual(result);
      });
    });

    it('should do nothing if none of the artifacts were selected', function() {
      controller.upload();
      $rootScope.$digest();
      expect(uploadToKgService.uploadSimulation).not.toHaveBeenCalled();
    });

    it('should set error if cannot upload simulation meta-data to KG', function() {
      controller.$scope.vm.selectedParent.files = [
        MOCKED_DATA.experimentFiles[0]
      ];
      controller.$scope.vm.selectFile(MOCKED_DATA.experimentFiles[0].uuid);

      uploadToKgService.uploadSimulation = jasmine
        .createSpy('uploadSimulation')
        .and.returnValue($q.reject());
      spyOn(controller, 'uploadKgArtifact');

      controller.upload();
      $rootScope.$digest();
      expect(controller.simulationUploadError).toBe(true);
      expect(controller.uploadKgArtifact).not.toHaveBeenCalled();
    });

    it('should set status to uploading', function() {
      controller.$scope.vm.selectedParent.files = [
        MOCKED_DATA.experimentFiles[0]
      ];
      controller.$scope.vm.selectFile(MOCKED_DATA.experimentFiles[0].uuid);

      spyOn(controller, 'uploadKgArtifact');

      controller.upload();
      $rootScope.$digest();
      expect(controller.status).toBe('uploading');
      expect(controller.$scope.vm.selectedParent.files[0].status).toBe(
        'uploading'
      );
      expect(controller.uploadKgArtifact).toHaveBeenCalled();
    });

    it("should set file status to noType if artifact's type was not selected", function() {
      const mockedFile = {
        uuid: '207a87c9-78d9-4504-bde7-6919feaac12a',
        name: 'joints.csv',
        status: ''
      };

      controller.$scope.vm.selectedParent.files = [mockedFile];
      controller.$scope.vm.selectFile(mockedFile.uuid);
      controller.$scope.vm.selectedParent.files[0].artifactType = '';

      spyOn(controller, 'uploadKgArtifact');

      controller.upload();
      $rootScope.$digest();
      expect(controller.$scope.vm.selectedParent.files[0].status).toBe(
        'noType'
      );
      expect(controller.uploadKgArtifact).not.toHaveBeenCalled();
    });

    it('should set file status to error if uploading fails', function() {
      uploadToKgService.uploadArtifact = jasmine
        .createSpy('uploadArtifact')
        .and.returnValue($q.reject());

      const mockedFile = {
        uuid: '207a87c9-78d9-4504-bde7-6919feaac12c',
        name: 'all_spikes.csv',
        status: '',
        artifactType: 'spikes'
      };

      controller.$scope.vm.selectedParent.files = [mockedFile];
      controller.$scope.vm.selectFile(mockedFile.uuid);

      controller.uploadKgArtifact(mockedFile);
      $rootScope.$digest();
      expect(mockedFile.status).toBe('error');
      expect(controller.status).toBe('selecting');
    });

    it('should set status to done when all files are uploaded', function() {
      uploadToKgService.uploadArtifact = jasmine
        .createSpy('uploadArtifact')
        .and.returnValue($q.resolve(MOCKED_DATA.simulation));

      storageServer.getBlobContent = jasmine
        .createSpy('getBlobContent')
        .and.returnValue($q.resolve({ data: 'data' }));

      const mockedFiles = [
        {
          uuid: '207a87c9-78d9-4504-bde7-6919feaac12a',
          name: 'joints.csv',
          status: '',
          artifactType: 'jointpoisitions'
        },
        {
          uuid: '207a87c9-78d9-4504-bde7-6919feaac12c',
          name: 'all_spikes.csv',
          status: '',
          artifactType: 'spikes'
        }
      ];

      controller.$scope.vm.selectedParent.files = [
        mockedFiles[0],
        mockedFiles[1]
      ];
      controller.$scope.vm.selectFile(mockedFiles[0].uuid);
      controller.$scope.vm.selectFile(mockedFiles[1].uuid);

      controller.uploadKgArtifact(mockedFiles[0]);
      $rootScope.$digest();
      controller.uploadKgArtifact(mockedFiles[1]);
      $rootScope.$digest();
      expect(controller.$scope.vm.selectedParent.files[0].status).toBe(
        'uploaded'
      );
      expect(controller.$scope.vm.selectedParent.files[1].status).toBe(
        'uploaded'
      );
      expect(controller.status).toBe('done');
    });
  });
})();
