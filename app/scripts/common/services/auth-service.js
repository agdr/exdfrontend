/**---LICENSE-BEGIN - DO NOT CHANGE OR MOVE THIS HEADER
 * This file is part of the Neurorobotics Platform software
 * Copyright (C) 2014,2015,2016,2017 Human Brain Project
 * https://www.humanbrainproject.eu
 *
 * The Human Brain Project is a European Commission funded project
 * in the frame of the Horizon2020 FET Flagship plan.
 * http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * ---LICENSE-END**/

/* global Keycloak */

let keycloakClient = undefined;

const INIT_CHECK_INTERVAL_MS = 100;
const INIT_CHECK_MAX_RETRIES = 290;

(function() {
  'use strict';

  class AuthService {
    constructor($window, $location, bbpConfig) {
      this.$window = $window;
      this.$location = $location;
      this.bbpConfig = bbpConfig;

      this.authConfig = this.bbpConfig.get('auth');
      this.proxyURL = this.bbpConfig.get('api.proxy.url');
      this.oidcEnabled = this.bbpConfig.get('auth.enableOIDC'); //this.authConfig && this.authConfig.enableOIDC;
      this.clientId = this.authConfig && this.authConfig.clientId;
      this.authURL = this.authConfig && this.authConfig.url;
      this.STORAGE_KEY = `tokens-${this
        .clientId}@https://iam.ebrains.eu/auth/realms/hbp`;

      this.init();
    }

    init() {
      this.initialized = false;
      if (this.oidcEnabled) {
        this.authCollab().then(() => {
          this.initialized = true;
        });
      } else {
        this.checkForNewLocalTokenToStore();
        this.initialized = true;
      }

      this.promiseInitialized = new Promise((resolve, reject) => {
        let numChecks = 0;
        let checkInterval = setInterval(() => {
          numChecks++;
          if (numChecks > INIT_CHECK_MAX_RETRIES) {
            clearInterval(checkInterval);
            reject();
          }
          if (this.initialized) {
            clearInterval(checkInterval);
            resolve();
          }
        }, INIT_CHECK_INTERVAL_MS);
      });
    }

    authenticate(config) {
      if (this.oidcEnabled) {
        this.authCollab(config);
      } else {
        this.authLocal(config);
      }
    }

    getToken() {
      if (this.oidcEnabled) {
        if (keycloakClient && keycloakClient.authenticated) {
          keycloakClient
            .updateToken(30)
            .then(function() {})
            .catch(function() {
              console.error('Failed to refresh token');
            });
          return keycloakClient.token;
        } else {
          console.error('getToken() - Client is not authenticated');
        }
      } else {
        return this.getStoredLocalToken();
      }
    }

    logout() {
      if (this.oidcEnabled) {
        if (keycloakClient && keycloakClient.authenticated) {
          keycloakClient.logout();
          keycloakClient.clearStoredLocalToken();
        } else {
          console.error('Client is not authenticated');
        }
      } else {
        return this.clearStoredLocalToken();
      }
    }

    authLocal(config) {
      if (this.authenticating) {
        return;
      }
      this.authenticating = true;

      this.authURL = this.authURL || config.url;
      this.clientId = this.clientId || config.clientId;

      let absoluteUrl = /^https?:\/\//i;
      if (!absoluteUrl.test(this.authURL))
        this.authURL = `${this.proxyURL}${this.authURL}`;

      this.clearStoredLocalToken();
      this.$window.location.href = `${this.authURL}&client_id=${this
        .clientId}&redirect_uri=${encodeURIComponent(location.href)}`;
    }

    checkForNewLocalTokenToStore() {
      const path = this.$location.url();

      const accessTokenMatch = /&access_token=([^&]*)/.exec(path);
      if (!accessTokenMatch || !accessTokenMatch[1]) return;

      let accessToken = accessTokenMatch[1];
      localStorage.setItem(
        this.STORAGE_KEY,
        //eslint-disable-next-line camelcase
        JSON.stringify([{ access_token: accessToken }])
      );

      // navigate to clean url
      let cleanedPath = path.substr(0, path.indexOf('&'));
      this.$location.url(cleanedPath);
    }

    clearStoredLocalToken() {
      localStorage.removeItem(this.STORAGE_KEY);
    }

    getStoredLocalToken() {
      let storedItem = localStorage.getItem(this.STORAGE_KEY);
      if (!storedItem) {
        // this token will be rejected by the server and the client will get a proper auth error
        return 'no-token';
      }

      try {
        let tokens = JSON.parse(storedItem);
        return tokens.length ? tokens[tokens.length - 1].access_token : null;
      } catch (e) {
        // this token will be rejected by the server and the client will get a proper auth error
        return 'malformed-token';
      }
    }

    authCollab(config) {
      if (this.authenticating) {
        return;
      }
      this.authenticating = true;

      return new Promise(resolve => {
        this.authURL = this.authURL || config.url;

        this.initKeycloakClient().then(() => {
          if (!keycloakClient.authenticated) {
            // User is not authenticated, run login
            keycloakClient
              .login({ scope: 'openid profile email group' })
              .then(() => {
                resolve(true);
              });
          } else {
            keycloakClient.loadUserInfo().then(userInfo => {
              this.userInfo = userInfo;
              resolve(true);
            });
          }
        });
      });
    }

    initKeycloakClient() {
      return new Promise(resolve => {
        keycloakClient = Keycloak({
          realm: 'hbp',
          clientId: this.clientId,
          //'public-client': true,
          'confidential-port': 0,
          url: this.authURL,
          redirectUri: window.location.href // 'http://localhost:9001/#/esv-private' //
        });

        keycloakClient
          .init({
            flow: 'hybrid' /*, responseMode: 'fragment'*/
          })
          .then(() => {
            resolve(keycloakClient);
          });
      });
    }
  }

  AuthService.$$ngIsClass = true;
  AuthService.$inject = ['$window', '$location', 'bbpConfig'];

  angular.module('authModule', []).service('authService', AuthService);
})();
