/**---LICENSE-BEGIN - DO NOT CHANGE OR MOVE THIS HEADER
 * This file is part of the Neurorobotics Platform software
 * Copyright (C) 2014,2015,2016,2017 Human Brain Project
 * https://www.humanbrainproject.eu
 *
 * The Human Brain Project is a European Commission funded project
 * in the frame of the Horizon2020 FET Flagship plan.
 * http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * ---LICENSE-END**/

(function() {
  'use strict';
  const NRP_STORAGE_IP = 'http://148.187.97.48';

  class UploadToKgController {
    constructor(
      $scope,
      $filter,
      $timeout,
      simulationInfo,
      experimentService,
      nrpUser,
      kgInterfaceService,
      storageServer,
      uploadToKgService
    ) {
      this.$scope = $scope;
      this.$filter = $filter;
      this.$timeout = $timeout;
      this.kgInterfaceService = kgInterfaceService;
      this.storageServer = storageServer;
      this.uploadToKgService = uploadToKgService;

      this.title = 'Upload CSVs to Knowledge Graph';
      this.uploadLabel = 'Upload';
      this.cancelLabel = 'Back';
      this.status = 'selecting';
      this.simulationUploadError = false;

      this.metaData = {
        experimentID: simulationInfo.experimentID,
        realTimeText: simulationInfo.realTimeText,
        owner: undefined,
        name: simulationInfo.experimentDetails.name,
        creationDate: moment(experimentService.creationDate)
          .utc()
          .format('YYYY-MM-DD HH:mm:ss')
      };

      nrpUser.getOwnerDisplayName('me').then(owner => {
        this.metaData.owner = owner;
      });
    }

    upload() {
      let isRequestCorrect = false;
      if (this.$scope.vm.selectedFilesIds.length) {
        this.$scope.vm.selectedParent.files.forEach(f => {
          if (
            f.status !== 'uploaded' &&
            this.$scope.vm.selectedFilesIds.includes(f.uuid)
          ) {
            isRequestCorrect = true;
          }
        });
      }
      if (!isRequestCorrect) {
        return;
      }

      let simulationBody = {
        '@type': 'https://schema.hbp.eu/sp10/Simulation',
        'http://schema.hbp.eu/sp10/id': this.metaData.experimentID,
        'http://schema.hbp.eu/sp10/launchedAt': this.metaData.creationDate,
        'http://schema.hbp.eu/sp10/owner': this.metaData.owner,
        'http://schema.hbp.eu/sp10/runtime': this.$filter('timeDDHHMMSS')(
          this.metaData.realTimeText
        ),
        'http://schema.hbp.eu/sp6/model': {
          '@id': this.uploadToKgService.knowledgeGraphBrainId
        },
        'http://schema.org/identifier': this.metaData.experimentID,
        'http://schema.org/name': this.metaData.name
      };

      this.uploadToKgService
        .uploadSimulation(simulationBody)
        .then(() => {
          this.simulationUploadError = false;
          this.$scope.vm.selectedParent.files.forEach(f => {
            if (
              f.status !== 'uploaded' &&
              this.$scope.vm.selectedFilesIds.includes(f.uuid)
            ) {
              if (f.artifactType === '') {
                f.status = 'noType';
              } else {
                f.status = 'uploading';
                this.status = 'uploading';
                this.uploadKgArtifact(f);
              }
            }
          });
        })
        .catch(() => {
          this.simulationUploadError = true;
        });
    }

    uploadKgArtifact(file) {
      const artifactType = file.artifactType;
      const fileType = file.name.split('.').pop();
      const capitalizedType =
        artifactType.charAt(0).toUpperCase() + artifactType.slice(1);
      let artifactBody = {
        '@type': 'https://schema.hbp.eu/sp10/' + capitalizedType,
        'http://schema.hbp.eu/sp10/file_url': 'To Be Updated',
        'http://schema.hbp.eu/sp10/simulation': {
          '@id': this.uploadToKgService.simulation['@id']
        },
        'http://schema.org/identifier': file.uuid,
        'http://schema.org/name': file.name
      };
      let idNumber = undefined;

      return this.uploadToKgService
        .uploadArtifact(artifactType, file.uuid, artifactBody)
        .then(res => {
          idNumber = res['@id'].split('/').pop();
          return this.storageServer.getBlobContent(
            this.$scope.vm.selectedParent.uuid,
            file.uuid
          );
        })
        .then(blobContent =>
          this.uploadToKgService.uploadAttachment(
            fileType,
            idNumber,
            blobContent.data
          )
        )
        .then(() => {
          artifactBody['http://schema.hbp.eu/sp10/file_url'] =
            NRP_STORAGE_IP +
            '/proxy/storage/knowledgeGraph/data/' +
            idNumber +
            '.' +
            fileType;
          return this.uploadToKgService.uploadArtifact(
            artifactType,
            file.uuid,
            artifactBody
          );
        })
        .then(() => {
          file.uploadedDate = this.uploadToKgService.getUploadedDate(file.uuid);
          file.status = 'uploaded';
        })
        .catch(() => {
          file.status = 'error';
        })
        .finally(() => {
          const uploading = this.$scope.vm.selectedParent.files.filter(
            f => f.status === 'uploading'
          ).length;
          const uploaded = this.$scope.vm.selectedParent.files.filter(
            f => f.status === 'uploaded'
          ).length;

          if (
            uploading === 0 &&
            uploaded < this.$scope.vm.selectedParent.files.length
          ) {
            this.status = 'selecting';
          }
          if (
            uploading === 0 &&
            uploaded === this.$scope.vm.selectedParent.files.length
          ) {
            this.status = 'done';
            this.$timeout(() => {
              this.$scope.$close();
            }, 1000);
          }
        });
    }
  }

  UploadToKgController.$$ngIsClass = true;
  UploadToKgController.$inject = [
    '$scope',
    '$filter',
    '$timeout',
    'simulationInfo',
    'experimentService',
    'nrpUser',
    'kgInterfaceService',
    'storageServer',
    'uploadToKgService'
  ];

  angular
    .module('uploadToKg', [])
    .controller('UploadToKgController', UploadToKgController);
})();
