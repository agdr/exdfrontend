/**---LICENSE-BEGIN - DO NOT CHANGE OR MOVE THIS HEADER
 * This file is part of the Neurorobotics Platform software
 * Copyright (C) 2014,2015,2016,2017 Human Brain Project
 * https://www.humanbrainproject.eu
 *
 * The Human Brain Project is a European Commission funded project
 * in the frame of the Horizon2020 FET Flagship plan.
 * http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * ---LICENSE-END**/

/* global Plotly: false */

(function() {
  'use strict';

  angular.module('exdFrontendApp').directive('plottingTool', [
    'STATE',
    'stateService',
    'backendInterfaceService',
    'roslib',
    'simulationInfo',
    'sceneInfo',
    '$timeout',
    'userInteractionSettingsService',
    function(
      STATE,
      stateService,
      backendInterfaceService,
      roslib,
      simulationInfo,
      sceneInfo,
      $timeout,
      userInteractionSettingsService
    ) {
      return {
        templateUrl: 'components/plotting-tool/plotting-tool.template.html',
        restrict: 'E',
        link: function(scope, element) {
          scope.stateService = stateService;
          scope.STATE = STATE;
          scope.plotVisible = false;
          scope.loadSettingsWhenTopicReady = true;
          scope.selectedPlot = null;
          scope.roslib = roslib;
          scope.simulationInfo = simulationInfo;
          scope.topics = {};
          scope.sortedTopics = [];
          scope.sceneInfo = sceneInfo;
          scope.topicConnections = [];
          scope.userInteractionSettingsService = userInteractionSettingsService;
          scope.plotContainer = element[0].getElementsByClassName(
            'plotlypane'
          )[0];

          scope.maxPointsInPlots = 500;
          scope.modelStateUpdateRate = 1.0 / 10.0;
          scope.modelStatePointsPerSec = 2;
          scope.stdTypeUpdateRate = 20;

          scope.supportedTypes = [
            'std_msgs/Float32',
            'std_msgs/Float64',
            'std_msgs/Int32',
            'std_msgs/Int64',
            'std_msgs/UInt32',
            'std_msgs/UInt64'
          ];

          document.addEventListener('contextmenu', event =>
            event.preventDefault()
          );

          scope.plotlyConfig = {
            displaylogo: false
          };

          scope.categories = [
            {
              title: 'Basic',
              thumbnail: 'img/esv/plotting-tool/basic_chart.png',
              models: [
                {
                  modelTitle: 'Line',
                  thumbnail: 'img/esv/plotting-tool/line_charts.png',
                  dimensions: 2,
                  multi: true, // We can add several lines
                  hasAxis: true,
                  type: 'scatter',
                  mode: 'lines'
                },
                {
                  modelTitle: 'Pie',
                  thumbnail: 'img/esv/plotting-tool/pie_charts.png',
                  dimensions: 1,
                  multi: true,
                  hasAxis: false,
                  type: 'pie',
                  mergedDimensions: true,
                  valuesMustBePositive: true
                },
                {
                  modelTitle: 'Scatter',
                  thumbnail: 'img/esv/plotting-tool/scatter_plots.png',
                  dimensions: 2,
                  multi: false,
                  hasAxis: true,
                  type: 'scatter',
                  mode: 'markers'
                },
                {
                  modelTitle: 'Bar',
                  thumbnail: 'img/esv/plotting-tool/bar_charts.png',
                  dimensions: 1,
                  multi: true,
                  hasAxis: false,
                  type: 'bar',
                  mergedDimensions: true,
                  mergedDimensionsUseXY: true
                },
                {
                  modelTitle: 'Fill Area',
                  thumbnail: 'img/esv/plotting-tool/filled_area.png',
                  dimensions: 2,
                  multi: true,
                  hasAxis: true,
                  type: 'scatter',
                  fill: 'tozeroy'
                },
                {
                  modelTitle: 'Bubbles',
                  thumbnail: 'img/esv/plotting-tool/bubble_chart.png',
                  dimensions: 3,
                  multi: true,
                  hasAxis: true,
                  type: 'scatter',
                  mode: 'markers',
                  lastDimensionIsSize: true
                }
              ]
            },
            {
              title: 'Stastical',
              thumbnail: 'img/esv/plotting-tool/statistical_chart.png',
              models: [
                {
                  modelTitle: 'Error Bars',
                  thumbnail: 'img/esv/plotting-tool/error_bars.png',
                  dimensions: 3,
                  multi: true,
                  hasAxis: true,
                  type: 'scatter',
                  mode: 'lines',
                  lastDimensionIsYError: true
                }
              ]
            },
            {
              title: '3D',
              thumbnail: 'img/esv/plotting-tool/3d_charts.png',
              models: [
                {
                  modelTitle: 'Scatter 3D',
                  thumbnail: 'img/esv/plotting-tool/scatter3d.png',
                  dimensions: 3,
                  multi: true,
                  hasAxis: true,
                  type: 'scatter3d',
                  mode: 'markers'
                }
              ]
            }
          ];

          scope.updatePlotly = function(dest) {
            Plotly.react(
              dest,
              scope.plotlyData,
              scope.plotlyLayout,
              scope.plotlyConfig
            );
          };

          scope.buildPlotly = function(dest, data, layout) {
            if (scope.plotly) {
              return Plotly.react(dest, data, layout, scope.plotlyConfig);
            }

            return Plotly.plot(dest, data, layout, scope.plotlyConfig);
          };

          scope.contextualUniqueKey = function() {
            return scope.findContextualUniqueKey(element[0], 'plotid');
          };

          scope.findContextualUniqueKey = function(base, rootKey) {
            if (
              !base.parentElement ||
              base.parentElement.id === 'golden-layout-container'
            ) {
              return rootKey;
            }

            const parentChildren = base.parentElement.children;
            let index = -1;

            for (let i = 0; i < parentChildren.length; i++) {
              if (parentChildren[i] === base) {
                index = i;
                break;
              }
            }

            rootKey += '-';
            rootKey += index;

            return scope.findContextualUniqueKey(base.parentElement, rootKey);
          };

          scope.plotUniqueKey = scope.contextualUniqueKey();

          scope.rosWebsocketURL =
            simulationInfo.serverConfig.rosbridge.websocket;
          scope.rosWebsocket = roslib.getOrCreateConnectionTo(
            scope.rosWebsocketURL
          );
          scope.rosModelPropertyService = roslib.createService(
            scope.rosWebsocket,
            '/gazebo/get_model_properties',
            'GetModelProperties'
          );

          scope.addTopic = function(topic, rosType) {
            if (scope.supportedTypes.includes(rosType)) {
              if (rosType === 'geometry_msgs.msg.Twist') {
                scope.topics[topic + '/linear.x'] = rosType;
                scope.topics[topic + '/linear.y'] = rosType;
                scope.topics[topic + '/linear.z'] = rosType;
                scope.topics[topic + '/angular.x'] = rosType;
                scope.topics[topic + '/angular.y'] = rosType;
                scope.topics[topic + '/angular.z'] = rosType;
              } else {
                scope.topics[topic] = rosType;
              }
            }
          };

          scope.loadRobotsTopics = function() {
            if (scope.sceneInfo.robots) {
              for (let i = 0; i < scope.sceneInfo.robots.length; i++) {
                const robot = scope.sceneInfo.robots[i];

                scope.topics['/' + robot.robotId + '/model_state/position.x'] =
                  'gazebo_msgs/ModelStates';
                scope.topics['/' + robot.robotId + '/model_state/position.y'] =
                  'gazebo_msgs/ModelStates';
                scope.topics['/' + robot.robotId + '/model_state/position.z'] =
                  'gazebo_msgs/ModelStates';
                scope.topics['/' + robot.robotId + '/model_state/angle.x'] =
                  'gazebo_msgs/ModelStates';
                scope.topics['/' + robot.robotId + '/model_state/angle.y'] =
                  'gazebo_msgs/ModelStates';
                scope.topics['/' + robot.robotId + '/model_state/angle.z'] =
                  'gazebo_msgs/ModelStates';

                /* eslint-disable camelcase */
                const request = new scope.roslib.ServiceRequest({
                  model_name: robot.robotId
                });
                /* eslint-enable camelcase */

                this.rosModelPropertyService.callService(
                  request,
                  success => {
                    scope.parseRobotModelTopics(success);
                  },
                  failure => {
                    console.info(failure);
                  }
                );
              }
            }
          };

          scope.loadTopics = function(response) {
            scope.topics = { Time: '_time' };

            for (let i = 0; i < response.topics.length; i++) {
              if (scope.supportedTypes.includes(response.topics[i].topicType)) {
                scope.topics[response.topics[i].topic] =
                  response.topics[i].topicType;
              }
            }

            scope.sortedTopics = Object.keys(scope.topics);
            scope.sortedTopics.sort();

            scope.loadRobotsTopics();

            if (scope.loadSettingsWhenTopicReady) {
              scope.loadSettingsWhenTopicReady = false;
              scope.loadSettings();
            }
          };

          scope.loadSettings = function() {
            scope.plotUniqueKey = scope.contextualUniqueKey();

            if (
              !scope.userInteractionSettingsService.settingsData
                .plottingToolsData ||
              !(
                scope.plotUniqueKey in
                scope.userInteractionSettingsService.settingsData
                  .plottingToolsData
              )
            ) {
              return;
            }

            const settings =
              scope.userInteractionSettingsService.settingsData
                .plottingToolsData[scope.plotUniqueKey];

            scope.structureSetupVisible = settings.structureSetupVisible;
            scope.plotVisible = settings.plotVisible;
            scope.axisLabels = settings.axisLabels;
            scope.plotModel = settings.plotModel;
            scope.plotStructure = settings.plotStructure;

            if (scope.plotVisible) {
              scope.showPlot(true);
            } else if (scope.structureSetupVisible) {
              scope.showStructureSetup(scope.plotModel);
            }
          };

          scope.saveSettings = function() {
            scope.plotUniqueKey = scope.contextualUniqueKey();

            if (
              !scope.userInteractionSettingsService.settingsData
                .plottingToolsData
            ) {
              scope.userInteractionSettingsService.settingsData.plottingToolsData = {};
            }

            let settings = {};

            settings.structureSetupVisible = scope.structureSetupVisible;
            settings.plotVisible = scope.plotVisible;
            settings.axisLabels = scope.axisLabels;
            settings.plotModel = scope.plotModel;
            settings.plotStructure = scope.plotStructure;

            scope.userInteractionSettingsService.settingsData.plottingToolsData[
              scope.plotUniqueKey
            ] = settings;
            scope.userInteractionSettingsService.saveSettings();
          };

          scope.updateVisibleModels = function() {
            scope.visibleModels = [];

            for (let i = 0; i < scope.categories.length; i++) {
              let cat = scope.categories[i];

              if (cat.visible) {
                for (let j = 0; j < cat.models.length; j++) {
                  cat.models[j].color = cat.color['default'];
                  scope.visibleModels.push(cat.models[j]);
                }
              }
            }
          };

          scope.checkSize = function(size) {
            return size < 280 ? 280 : size;
          };

          scope.addDefaultStructureElement = function() {
            let minimalPlot = { label: '', dimensions: [] };
            for (let i = 0; i < scope.plotModel.dimensions; i++) {
              scope.plotStructure.axis.push('');
              minimalPlot.dimensions.push({ source: scope.sortedTopics[i] });
            }

            scope.plotStructure.plotElements.push(minimalPlot);
          };

          scope.removeSourceElement = function(index) {
            scope.plotStructure.plotElements.splice(index, 1);
          };

          scope.showStructureSetup = function(model) {
            scope.structureSetupVisible = true;

            scope.axisLabels = ['X', 'Y', 'Z'];
            scope.plotModel = model;
            scope.plotStructure = { axis: [], plotElements: [] };

            backendInterfaceService.getTopics(scope.loadTopics);

            while (scope.plotModel.dimensions < scope.axisLabels.length)
              scope.axisLabels.pop();

            scope.addDefaultStructureElement();
          };

          scope.newPlot = function() {
            scope.plotVisible = false;
            scope.structureSetupVisible = false;

            if (scope.unregisterWatch) {
              scope.unregisterWatch();
              scope.unregisterWatch = null;
            }

            scope.stopListening();
            scope.saveSettings();
          };

          scope.showPlot = function(dontSaveSettings) {
            if (scope.unregisterWatch) {
              scope.unregisterWatch();
              scope.unregisterWatch = null;
            }

            scope.plotVisible = true;
            scope.structureSetupVisible = false;

            let plotlyDest = scope.plotContainer;

            scope.plotlyLayout = {
              title: 'NRP plotting tool',
              width: scope.checkSize(plotlyDest.clientWidth),
              height: scope.checkSize(plotlyDest.clientHeight)
            };

            if (scope.plotModel.hasAxis) {
              for (
                let axis = 0;
                axis < scope.plotStructure.axis.length;
                axis++
              ) {
                let axisTitle = {
                  title: { text: scope.plotStructure.axis[axis] }
                };

                switch (axis) {
                  case 0:
                    scope.plotlyLayout.xaxis = axisTitle;
                    break;
                  case 1:
                    scope.plotlyLayout.yaxis = axisTitle;
                    break;
                  case 2:
                    scope.plotlyLayout.zaxis = axisTitle;
                    break;
                }
              }
            }

            scope.plotlyData = [];
            let newElement;

            for (
              let element = 0;
              element < scope.plotStructure.plotElements.length;
              element++
            ) {
              let label = scope.plotStructure.plotElements[element].label;

              if (scope.plotModel.mergedDimensions) {
                if (element === 0) {
                  newElement = {};

                  newElement.type = scope.plotModel.type;
                  newElement.mode = scope.plotModel.mode;

                  if (scope.plotModel.mergedDimensionsUseXY) {
                    newElement.x = [label];
                    newElement.y = [0.0];
                  } else {
                    newElement.values = [0.001];
                    newElement.labels = [label];
                  }

                  scope.plotlyData.push(newElement);
                } else {
                  if (scope.plotModel.mergedDimensionsUseXY) {
                    newElement.x.push(label);
                    newElement.y.push(0.0);
                  } else {
                    newElement.labels.push(label);
                    newElement.values.push(0.001);
                  }
                }
              } else {
                newElement = {};

                newElement.type = scope.plotModel.type;
                newElement.mode = scope.plotModel.mode;
                newElement.fill = scope.plotModel.fill;

                if (label.length > 0) {
                  newElement.name = label;
                  newElement.showlegend = true;
                } else {
                  newElement.showlegend = false;
                }

                for (
                  let dimension = 0;
                  dimension <
                  scope.plotStructure.plotElements[element].dimensions.length;
                  dimension++
                ) {
                  // Empty values for now, will be updated by messages

                  switch (dimension) {
                    case 0:
                      newElement.x = [];
                      break;
                    case 1:
                      newElement.y = [];
                      break;
                    case 2:
                      if (scope.plotModel.lastDimensionIsSize) {
                        // eslint-disable-next-line camelcase
                        newElement.marker = {
                          type: 'data',
                          size: [],
                          visible: true
                        };
                      } else if (scope.plotModel.lastDimensionIsYError) {
                        // eslint-disable-next-line camelcase
                        newElement.error_y = {
                          type: 'data',
                          array: [],
                          visible: true
                        };
                      } else {
                        newElement.z = [];
                      }
                      break;
                  }
                }

                scope.plotlyData.push(newElement);
              }
            }

            scope.plotly = scope.buildPlotly(
              plotlyDest,
              scope.plotlyData,
              scope.plotlyLayout
            );

            scope.startListening();

            if (!dontSaveSettings) {
              scope.saveSettings();
            }

            scope.unregisterWatch = scope.$watch(
              function() {
                return [plotlyDest.offsetWidth, plotlyDest.offsetHeight].join(
                  'x'
                );
              },
              function() {
                let plotlyDest = scope.plotContainer;

                Plotly.relayout(plotlyDest, {
                  width: scope.checkSize(plotlyDest.clientWidth),
                  height: scope.checkSize(plotlyDest.clientHeight)
                });

                if (scope.plotUniqueKey !== scope.contextualUniqueKey()) {
                  scope.saveSettings();
                }
              }
            );
          };

          scope.$on('$destroy', function() {
            if (scope.unregisterWatch) {
              scope.unregisterWatch();
              scope.unregisterWatch = null;
            }

            $timeout.cancel(scope.periodicalTimeout);
            scope.periodicalTimeout = null;

            scope.stopListening();

            scope.saveSettings();
          });

          scope.startListening = function() {
            scope.stopListening();

            let server = simulationInfo.serverConfig.rosbridge.websocket;

            scope.rosConnection = scope.roslib.getOrCreateConnectionTo(server);

            let topicSubsribed = {};

            for (
              let element = 0;
              element < scope.plotStructure.plotElements.length;
              element++
            ) {
              for (
                let dimension = 0;
                dimension <
                scope.plotStructure.plotElements[element].dimensions.length;
                dimension++
              ) {
                let topicName =
                  scope.plotStructure.plotElements[element].dimensions[
                    dimension
                  ].source;

                let topicType = scope.topics[topicName];

                if (topicType === '_time') continue;

                if (topicType === 'gazebo_msgs/ModelStates') {
                  topicName = '/gazebo/model_states';
                }

                if (!(topicName in topicSubsribed)) {
                  let topicSubscriber = scope.roslib.createTopic(
                    scope.rosConnection,
                    topicName,
                    topicType,
                    topicType === 'gazebo_msgs/ModelStates'
                      ? {
                          // eslint-disable-next-line camelcase
                          throttle_rate:
                            1.0 / scope.modelStatePointsPerSec * 1000.0
                        }
                      : undefined
                  );

                  topicSubsribed[topicName] = true;

                  if (topicType === 'gazebo_msgs/ModelStates') {
                    topicSubscriber.subscribe(scope.parseModelStateMessages);
                  } else {
                    topicSubscriber.subscribe(scope.parseStandardMessages);
                  }

                  scope.topicConnections.push(topicSubscriber);
                }
              }
            }
          };

          scope.stopListening = function() {
            for (let i in scope.topicConnections) {
              let connection = scope.topicConnections[i];
              connection.unsubscribe(scope.parseModelStateMessages);
              connection.unsubscribe(scope.parseStandardMessages);
            }

            delete scope.topicConnections;
            delete scope.rosConnection;

            scope.topicConnections = [];
          };

          scope.parseModelStateMessages = function(message) {
            let currentTime = Date.now() / 1000.0;

            if (
              scope.modelStateLastTime !== undefined &&
              currentTime - scope.modelStateLastTime <
                scope.modelStateUpdateRate
            )
              return;
            scope.modelStateLastTime = currentTime;

            let needUpdateTime = false;

            if (scope.plotlyData === null) return;
            for (let i = 0; i < scope.plotStructure.plotElements.length; i++) {
              let dataElement = scope.plotModel.mergedDimensions
                ? scope.plotlyData[0]
                : scope.plotlyData[i];

              for (
                let di = 0;
                di < scope.plotStructure.plotElements[i].dimensions.length;
                di++
              ) {
                let dimension =
                  scope.plotStructure.plotElements[i].dimensions[di];

                if (
                  scope.topics[dimension.source] === 'gazebo_msgs/ModelStates'
                ) {
                  // Search a message that matches (if any)

                  for (let j = 0; j < message.name.length; j++) {
                    if (dimension.source.startsWith('/' + message.name[j])) {
                      let addValue = false;
                      let value = 0;

                      if (
                        dimension.source.startsWith(
                          '/' + message.name[j] + '/model_state/position'
                        )
                      ) {
                        addValue = true;

                        if (dimension.source.endsWith('.x'))
                          value = message.pose[j].position.x;
                        else if (dimension.source.endsWith('.y'))
                          value = message.pose[j].position.y;
                        else if (dimension.source.endsWith('.z'))
                          value = message.pose[j].position.z;
                      } else if (
                        dimension.source.startsWith(
                          '/' + message.name[j] + '/model_state/angle'
                        )
                      ) {
                        let q = new THREE.Quaternion(
                          message.pose[j].orientation.x,
                          message.pose[j].orientation.y,
                          message.pose[j].orientation.z,
                          message.pose[j].orientation.w
                        );
                        let euler = new THREE.Euler();
                        euler.setFromQuaternion(q, 'XYZ');

                        addValue = true;

                        if (dimension.source.endsWith('.x')) value = euler.x;
                        else if (dimension.source.endsWith('.y'))
                          value = euler.y;
                        else if (dimension.source.endsWith('.z'))
                          value = euler.z;
                      }

                      if (addValue) {
                        scope.needPlotUpdate = true;
                        needUpdateTime = true;

                        scope.addValueToDimension(i, di, dataElement, value);
                      }
                      break;
                    }
                  }
                }
              }
            }

            if (needUpdateTime) {
              scope.addTimePoint();
            }
          };

          scope.addValueToDimension = function(
            elementIndex,
            di,
            dataElement,
            value
          ) {
            let plotValues;

            if (scope.plotModel.valuesMustBePositive) {
              value = value < 0 ? -value : value;
            }

            if (scope.plotModel.mergedDimensions) {
              if (scope.plotModel.mergedDimensionsUseXY) {
                plotValues = dataElement.y.slice(0);
                plotValues[elementIndex] = value;
                dataElement.y = plotValues;
              } else {
                plotValues = dataElement.values.slice(0);
                plotValues[elementIndex] = value;
                dataElement.values = plotValues;
              }
            } else {
              switch (di) {
                case 0:
                  plotValues = dataElement.x.slice(0);
                  plotValues.push(value);
                  if (plotValues.length > scope.maxPointsInPlots) {
                    plotValues.splice(
                      0,
                      plotValues.length - scope.maxPointsInPlots
                    );
                  }
                  dataElement.x = plotValues;
                  break;

                case 1:
                  plotValues = dataElement.y.slice(0);
                  plotValues.push(value);
                  if (plotValues.length > scope.maxPointsInPlots) {
                    plotValues.splice(
                      0,
                      plotValues.length - scope.maxPointsInPlots
                    );
                  }

                  dataElement.y = plotValues;
                  break;

                case 2:
                  if (scope.plotModel.lastDimensionIsSize) {
                    // eslint-disable-next-line camelcase
                    plotValues = dataElement.marker.size.slice(0);
                  } else if (scope.plotModel.lastDimensionIsYError) {
                    // eslint-disable-next-line camelcase
                    plotValues = dataElement.error_y.array.slice(0);
                  } else {
                    plotValues = dataElement.z.slice(0);
                  }

                  plotValues.push(value);
                  if (plotValues.length > scope.maxPointsInPlots) {
                    plotValues.splice(
                      0,
                      plotValues.length - scope.maxPointsInPlots
                    );
                  }

                  if (scope.plotModel.lastDimensionIsSize)
                    // eslint-disable-next-line camelcase
                    dataElement.marker.size = plotValues;
                  else if (scope.plotModel.lastDimensionIsYError) {
                    // eslint-disable-next-line camelcase
                    dataElement.error_y.array = plotValues;
                  } else {
                    dataElement.z = plotValues;
                  }

                  break;
              }
            }
          };

          scope.addTimePoint = function() {
            for (let i = 0; i < scope.plotStructure.plotElements.length; i++) {
              let dataElement = scope.plotModel.mergedDimensions
                ? scope.plotlyData[0]
                : scope.plotlyData[i];

              for (
                let di = 0;
                di < scope.plotStructure.plotElements[i].dimensions.length;
                di++
              ) {
                let dimension =
                  scope.plotStructure.plotElements[i].dimensions[di];

                if (scope.topics[dimension.source] === '_time') {
                  scope.addValueToDimension(
                    i,
                    di,
                    dataElement,
                    scope.simulationInfo.simulationTimeText
                  );
                }
              }
            }
          };

          scope.onTimeout = function() {
            if (scope.needPlotUpdate) {
              scope.needPlotUpdate = false;
              let plotlyDest = scope.plotContainer;

              scope.updatePlotly(plotlyDest);
            }

            scope.periodicalTimeout = $timeout(scope.onTimeout, 500);
          };

          scope.periodicalTimeout = $timeout(scope.onTimeout, 500);

          scope.parseStandardMessages = function(message) {
            let needUpdateTime = false;

            if (scope.plotlyData === null) return;

            for (let i = 0; i < scope.plotStructure.plotElements.length; i++) {
              let dataElement = scope.plotModel.mergedDimensions
                ? scope.plotlyData[0]
                : scope.plotlyData[i];

              for (
                let di = 0;
                di < scope.plotStructure.plotElements[i].dimensions.length;
                di++
              ) {
                let dimension =
                  scope.plotStructure.plotElements[i].dimensions[di];

                if (dimension.source === this.name) {
                  scope.addValueToDimension(i, di, dataElement, message.data);
                  scope.needPlotUpdate = true;
                  needUpdateTime = true;
                }
              }
            }

            if (needUpdateTime) {
              scope.addTimePoint();
            }
          };

          scope.toggleVisibleCategory = function(category) {
            category.visible = !category.visible;
            scope.updateVisibleModels();
          };

          scope.isCategoryVisible = function(category) {
            const categoryFound = scope.categories.find(
              cat => cat.title === category
            );
            return !!categoryFound && categoryFound.visible;
          };

          scope.createModelsCategories = function() {
            for (let i = 0; i < scope.categories.length; i++) {
              scope.categories[i].visible = i === 0;
              scope.categories[i].colorMode = 'default';
              scope.categories[i].color = {};
              scope.categories[i].color.default =
                'hsl(' +
                (10 + i / (scope.categories.length + 1) * 360.0) +
                ',95%,87%)';
              scope.categories[i].color.mouseover =
                'hsl(' +
                (10 + i / (scope.categories.length + 1) * 360.0) +
                ',80%,90%)'; // Mouse over
              scope.categories[i].color.mousedown =
                'hsl(' +
                (10 + i / (scope.categories.length + 1) * 360.0) +
                ',100%,70%)'; // Mouse down
            }
          };

          scope.parseRobotModelTopics = function(rosRobotProperties) {
            /* eslint-disable camelcase */

            if (
              rosRobotProperties.sensor_names &&
              rosRobotProperties.sensor_names.length > 0
            ) {
              for (
                let i = 0;
                i < rosRobotProperties.sensor_names.length;
                i = i + 1
              ) {
                if (
                  rosRobotProperties.sensor_ros_message_type &&
                  rosRobotProperties.sensor_ros_message_type[i] &&
                  rosRobotProperties.sensor_names_ROS.length ===
                    rosRobotProperties.sensor_names.length
                ) {
                  let topicURL = rosRobotProperties.rostopic_sensor_urls[i];

                  let rosTopic = topicURL;
                  let rosType = rosRobotProperties.sensor_ros_message_type[
                    i
                  ].replace('/', '.msg.');

                  if (scope.supportedTypes.includes(rosType)) {
                    scope.addTopic(rosTopic, rosType);
                  }
                }
              }
            }

            if (
              rosRobotProperties.rostopic_actuator_urls &&
              rosRobotProperties.rostopic_actuator_urls.length > 0
            ) {
              for (
                let i = 0;
                i < rosRobotProperties.rostopic_actuator_urls.length;
                i = i + 1
              ) {
                if (
                  rosRobotProperties.actuator_ros_message_type &&
                  rosRobotProperties.actuator_ros_message_type[i]
                ) {
                  let topicURL = rosRobotProperties.rostopic_actuator_urls[i];
                  let rosTopic = '/' + topicURL;
                  let rosType = rosRobotProperties.actuator_ros_message_type[
                    i
                  ].replace('/', '.msg.');

                  if (scope.supportedTypes.includes(rosType)) {
                    scope.addTopic(rosTopic, rosType);
                  }
                }
              }
            }
            /* eslint-enable camelcase */

            scope.sortedTopics = Object.keys(scope.topics);
            scope.sortedTopics.sort();
          };

          scope.$on('ROBOT_LIST_UPDATED', () => {
            scope.loadRobotsTopics();
          });

          scope.userInteractionSettingsService.settings.then(() => {
            backendInterfaceService.getTopics(scope.loadTopics);
          });

          scope.createModelsCategories();
          scope.updateVisibleModels();
        }
      };
    }
  ]);
})();
